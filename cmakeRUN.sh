#! /bin/bash
mkdir build -p
cd build
cmake -DCMAKE_TOOLCHAIN_FILE=../cmake/stm32.cmake ..
make stm32test.bin

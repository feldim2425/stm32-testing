cmake_minimum_required(VERSION 3.3.2)

set(SOURCE "${CMAKE_CURRENT_LIST_DIR}/Source")

if(NOT FreeRTOS_HEAP)
	set(FreeRTOS_HEAP "1" CACHE STRING "FreeRTOS Heap implementation")
endif()

if(NOT FreeRTOS_COMPILER)
	set(FreeRTOS_COMPILER "GCC" CACHE STRING "FreeRTOS Compiler. Required to set correct hardware implementation")
endif()

if(NOT FreeRTOS_ARCH)
	set(FreeRTOS_ARCH "Atmega88" CACHE STRING "FreeRTOS Architecture. Required to set correct hardware implementation")
endif()

if((FreeRTOS_HEAP LESS "1") OR (FreeRTOS_HEAP GREATER "5"))
	message(SEND_ERROR "FreeRTOS: Heap implementation has to be in range 1-5")
endif((FreeRTOS_HEAP LESS "1") OR (FreeRTOS_HEAP GREATER "5"))

message(STATUS "FreeRTOS: Heap implementation ${FreeRTOS_HEAP}")

set(CFILES "")
list(APPEND CFILES "${SOURCE}/tasks.c" "${SOURCE}/queue.c" "${SOURCE}/list.c" "${SOURCE}/portable/MemMang/heap_${FreeRTOS_HEAP}.c" "${SOURCE}/portable/${FreeRTOS_COMPILER}/${FreeRTOS_ARCH}/port.c")

foreach(_comp ${FreeRTOS_FIND_COMPONENTS})
  if("SoftTMR" STREQUAL ${_comp})
	set(FreeRTOS_SOFTTMR ON)
  elseif("Event_Groups" STREQUAL ${_comp})
	set(FreeRTOS_EVENTGROUP ON)
  elseif("Stream_Buffer" STREQUAL ${_comp})
	set(FreeRTOS_STREAMBUF ON)
  else()
  message(STATUS "NONE ${_comp}")
	set(FreeRTOS_FOUND OFF)
    message(SEND_ERROR "FreeRTOS: Unsupported component: ${_comp}")
  endif()
endforeach()

if(FreeRTOS_SOFTTMR)
	list(APPEND CFILES "${SOURCE}/timers.c")
endif(FreeRTOS_SOFTTMR)

if(FreeRTOS_EVENTGROUP)
	list(APPEND CFILES "${SOURCE}/event_groups.c")
endif(FreeRTOS_EVENTGROUP)

if(FreeRTOS_STREAMBUF)
	list(APPEND CFILES "${SOURCE}/stream_buffer.c")
endif(FreeRTOS_STREAMBUF)

FILE(GLOB CFILES_PORT ${SOURCE}/portable/${FreeRTOS_COMPILER}/${FreeRTOS_ARCH}/*.c)
list(APPEND CFILES ${CFILES_PORT})

add_library(FreeRTOS ${CFILES})

#SET_TARGET_PROPERTIES(freeRtos PROPERTIES LINKER_LANGUAGE C)

target_include_directories(FreeRTOS PUBLIC ${SOURCE}/include)
target_include_directories(FreeRTOS PUBLIC ${SOURCE}/portable/${FreeRTOS_COMPILER}/${FreeRTOS_ARCH})

target_link_libraries(FreeRTOS)

set(FreeRTOS_INCLUDE_DIRS "${SOURCE}/include;${SOURCE}/portable/${FreeRTOS_COMPILER}/${FreeRTOS_ARCH}")
